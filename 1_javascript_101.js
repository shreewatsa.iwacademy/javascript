#!/usr/bin node   //hashbang

("use strict");

// console.log("hello world");
function greetme(yourname) {
  console.log("Hello " + yourname);
}
greetme("Javascript");

/* This 
is a
multiline comment.
*/

// Note: Identifier start with letter, _ or $ .
const PI = 3.14; // read-only constant, block-scope .
var x = 42; // local or global
let y = 43; // block-scope variable
z = 44; //undeclared global variable

// undefined, behaves as false in boolean context.
let m; // m = undefined
if (m === undefined) {
  console.log(`Variable m is ${m}`);
}

if (!m) console.log("m is undefined."); // single line if statement.
console.log(m + 20); // NaN , undefined value converts to NaN when used in numeric context.
// console.log(newVariable); // ReferenceError

// null , behaves as 0 in numeric context and false in boolean contexts.
var n = null;

// Functions
function foo() {}
var baz = function () {};

// Global variable
// eg : window.varaible, etc.
// global object is window.

// Const variable : properties of objects/arrays assigned to constants are not protected.
const My_obj = { key: "value" };
My_obj.key = "othervalue";
console.log(My_obj);

// Data Types:
// Boolean, null, undefined, Number, BigInt, String, Symbol, Object.
// Number : an integer or a floating point number
// BigInt : an integer with arbitrary precision. 900719925474992n.
// Symbol : A data type whose instances are unique and immutable.

// parseInt(), parseFloat()

// Literals: (Literals are fixed values, not variables.)
// Array, Boolean, Floating-point, Numeric, Object, RegExp, String literals.

// Array literal : Trailing comma is ignored.
let coffees = ["French Roast", "Colombian", , "Kona"]; // coffees[2] is undefined.
coffees.sort();
console.log(coffees);

// Numeric literals : Decimal(base 10), Octal(8), Hexadecimal(16) and binary(2).
// leading 0 or 0o or 0O : octal
// leading 0x or 0X : hexadecimal
// leading 0b or 0B : binary

// Boolean literal : eg, Boolean(2+5), Boolean(1-1) , etc.
// Floating point literal : eg, -3.15E+12, .1e-23, -.123456789, etc.
// Object literal : {key : value}

var sales = "Toyota";

function carTypes(name) {
  if (name === "Honda") {
    return name;
  } else {
    return "Sorry, we don't sell " + name + ".";
  }
}

var car = { myCar: "Saturn", getCar: carTypes("Honda"), 7: sales };

console.log(car.myCar, car["getCar"], car[7]); // Saturn Honda Toyota

// Advanced object literals
var obj = {
  // __proto__
  __proto__: String,
  // Shorthand for ‘handler: handler’
  greetme,
  // Methods
  toString() {
    // Super calls
    return "d " + super.toString();
  },
  // Computed (dynamic) property names
  ["prop_" + (() => 42)()]: 42,
};

// RegExp literals: eg var re = /ab+c/
// String literal : 'abc', etc
console.log(`Length of 'abc' is ${"abc".length}`);

// Escaping characters
// For the following example, always use template literal .
var str = "this string \
is broken \
across multiple \
lines.";
console.log(str); // this string is broken across multiple lines.

// array.length : to get the length of array.

// Note: operations of a number with undefined gives NaN.
