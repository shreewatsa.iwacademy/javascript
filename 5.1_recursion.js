// Nature of recursion.
function foo(i) {
  if (i < 0) return;
  console.log("begin: " + i);
  foo(i - 1);
  console.log("end: " + i);
}
foo(3);

// Converting loop to recursive function.

// Loop
var x = 0;
while (x < 5) {
  // "x < 5" is the loop condition
  // do stuff
  console.log(x);
  x++;
}

// Equivalent recursive function
function loop(x) {
  if (x >= 5)
    // "x >= 5" is the exit condition (equivalent to "!(x < 5)")
    return;
  // do stuff
  console.log(x);
  loop(x + 1); // the recursive call
}
loop(0);
