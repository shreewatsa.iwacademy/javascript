//A function defined in the global scope can access all variables defined in the global scope.
// A function defined inside another function can also access all variables defined in its parent function,
// and any other variables to which the parent function has access.
// The following variables are defined in the global scope

var num1 = 20,
  num2 = 3,
  name = "Chamahk";

// This function is defined in the global scope
function multiply() {
  return num1 * num2;
}

multiply(); // Returns 60

// A nested function example
function getScore() {
  var num1 = 2,
    num2 = 3;

  function add() {
    return name + " scored " + (num1 + num2);
  }

  return add();
}

getScore(); // Returns "Chamahk scored 5"
