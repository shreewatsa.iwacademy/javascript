// Working with objects in JS.

var myCar = new Object();
myCar.make = "Ford";
myCar.model = "Mustang";
myCar.year = 1969;
console.log(myCar);

var myBus = {
  make: "Toyota",
  model: "B56",
  year: 2015,
};
console.log(myBus);

// Unassigned properties of an object are undefined (and not null).
console.log(myBus.owner);

// Accessing or setting using bracket notation.
myCar["make"] = "Ford";
myCar["model"] = "Mustang";
myCar["year"] = 1969;

// object property name can be any valid JS string, or anything that can be
// converted to a string.
// All the keys are eventually converted to string to be used as a key.

// four variables are created and assigned in a single go,
// separated by commas
var myObj = new Object(),
  str = "myString",
  rand = Math.random(),
  obj = new Object();

myObj.type = "Dot syntax";
myObj["date created"] = "String with space";
myObj[str] = "String value";
myObj[rand] = "Random Number";
myObj[obj] = "Object"; // obj.toString() is called to set the key.
myObj[""] = "Even an empty string";

console.log(myObj);

//Enumerate the properties of an object: 3 ways.
// First way : for...in loop to iterate over all the enumerable properties of an object.
(function showProps(obj, objName) {
  var result = ``;
  for (var i in obj) {
    // obj.hasOwnProperty() : used to filter out properties from object's prototype chain
    if (obj.hasOwnProperty(i)) {
      result += `${objName}.${i} = ${obj[i]}\n`;
    }
  }
  console.log(result);
  return result;
})(myCar, "myCar");

// Second way: Object.keys(obj) :
// returns  array of all own (not in the prototype chain) enumerable properties of object.
console.log(Object.keys(myCar));

// Third way: Object.getOwnPropertyNames(obj)
// returns array of all own peoperties (enumerable or not) of an object.
console.log(Object.getOwnPropertyNames(myCar));

// List all properties
// Display hidden properties , and properties in prototype chain.
(function listAllProperties(o) {
  var objectToInspect;
  var result = [];

  for (
    objectToInspect = o;
    objectToInspect !== null;
    objectToInspect = Object.getPrototypeOf(objectToInspect)
  ) {
    result = result.concat(Object.getOwnPropertyNames(objectToInspect));
  }
  console.log(result);
  return result;
})(myCar);

// Creating objects
// First : using {}
// let myobj = { key: value}
// key : identifier or number or string literal or expression that evaluates to string.
// value: expression or value.

//Note: wrapping parenthesis around object is fine , eg : car = ({ owner: "John" })

// Second : using constructor function
function Car(make, model, year, owner) {
  this.make = make;
  this.model = model;
  this.year = year;
  this.owner = owner;
}

function Person(name, age, sex) {
  this.name = name;
  this.age = age;
  this.sex = sex;
}

var ken = new Person("Ken Jones", 39, "M");

var mycar = new Car("Eagle", "Talon TSi", 1993, ken);
mycar.color = "black";
var nissancar = new Car("Nissan", "300ZX", 1992);

console.log(mycar.owner.name);
console.log(mycar, nissancar);

// Third way : Object.create()
var Animal = {
  type: "Invertebrates", // Default value of properties
  displayType: function () {
    // Method which will display type of Animal
    console.log(this.type);
  },
};

// Create new animal type called animal1
var animal1 = Object.create(Animal);
animal1.displayType(); // Output:Invertebrates

// Create new animal type called Fishes
var fish = Object.create(Animal);
fish.type = "Fishes";
fish.displayType(); // Output:Fishes
