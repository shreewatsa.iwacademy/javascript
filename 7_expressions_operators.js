// Operators
// Assignment, Comparison, Arithmetic, Bitwise, Logical,
// String, Ternery, Comma, Unary and relational operators
x = 10;
y = 3;
x = x % y; // remainder, x %=y is the shortcut.
x **= y; // exponentiation
x &= y; // bitwise AND. x = x & y .
x = x ^ y; // bitwise XOR. x |= y for bitwise OR. x ~= x for bitwise NOT.

// Destructuring : arrays or objects.
let foo = ["one", "two", "three"];

// without destructuring
var one = foo[0];
var two = foo[1];
var three = foo[2];

// with destructuring
var [one, two, three] = foo;

// Comparison operators.
// String are compared based on standard lexicographical ordering , ie dictionary like.
// Strict equal : === , Strict unequal : !== .

// Numeric operators.
// divide by zero gives Infinity.

// Logical Opeartors : &&, ||, ! .
// Note: && and || operators actually return one of the operand.
// Operand can be boolean value or expression that ealuates to boolean value.
// null, 0, NaN, "" , undefined are falsy values.

// Note: logical expressions are evaluated left to right.
// false && anything is short-circuit evaluated to false.
// true || anything is short-circuit evaluated to true.

// Nulllish coalescing operator, ??
// works like || (OR), but only returns second operand if first operand is null or undefined.
// let myAddr = null ?? "Unknown Address";
// console.log(myAddr);

let mystring = "alpha";
mystring += "bet";
console.log(mystring);

// Ternery operator
let age = 20;
let status = age > 18 ? "adult" : "minor";
console.log(status);

// Comma operator: especially used inside for loop .
var x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var a = [x, x, x, x, x];

for (var i = 0, j = 9; i <= j; i++, j--)
  console.log("a[" + i + "][" + j + "]= " + a[i][j]);

// Unary operator, eg delete
// The delete operator deletes an object,
// an object's property, or an element at a specified index in an array.
// Variables declared with var cannot be deleted by delete operator.
// delete cannot delete predefined properties like Math.PI, etc.
// sets the property or element to undefined.
// delete operator returns true if successful , else false .

// delete objectName;
// delete objectName.property;
// delete objectName[index];
// delete property; // legal only within a with statement

x = 42;
var trees = ["redwood", "bay", "cedar", "oak", "maple"];
myobj = new Number();
myobj.h = 4; // create property h
delete x; // returns true (can delete if declared implicitly)
delete trees[3]; // returns false (cannot delete if declared with var)
delete Math.PI; // returns false (cannot delete predefined properties)
delete myobj.h; // returns true (can delete user-defined properties)
delete myobj; // returns true (can delete if declared implicitly)

trees[3] = undefined;
console.log(3 in trees); // true

// typeof operator : returns string of type( function, string, number, boolean, object, undefined, ..)
// Usage : typeof operand
typeof null; // object
typeof new Date(); // object
typeof Date; // function
typeof Function; // function
typeof String; // function

// void operator : specifies an expression to be evaluated without returning a value.
// Usage: void expression
// <a href="javascript:void(0)">Click here to do nothing</a>;
// <a href="javascript:void(document.form.submit())">Click here to submit</a>;

// Relational operators
// Usage: (propNameOrNumber in objectName) :: where propNameOrNumber is a string, numeric,
// or symbol expression representing a property name or array index, and objectName is
// the name of an object.
// Arrays
var trees = ["redwood", "bay", "cedar", "oak", "maple"];
3 in trees; // returns true
6 in trees; // returns false
"bay" in trees; // returns false (you must specify the index number, not the value at that index)
"length" in trees; // returns true (length is an Array property)

// built-in objects
"PI" in Math; // returns true
var myString = new String("coral");
"length" in myString; // returns true

// Custom objects
var mycar = { make: "Honda", model: "Accord", year: 1998 };
"make" in mycar; // returns true
"model" in mycar; // returns true

// instanceof operator
// Usage : objectName instanceof objectType
var theDay = new Date(1995, 12, 17);
if (theDay instanceof Date) {
  // statements to execute
}

// Expressions
// Primary expressions : this
// this keyword is used to refer to the current object.
// Usage: this['propertyName] or this.propertyName

// Left hand side expressions: spread operator, new operator, super keyword.

// var objectName = new objectType([param1, param2, ..., paramN]);

// super: call functions on an object's parent, call parent constructor.
// super([arguments]); // calls the parent constructor.
// super.functionOnParent([arguments]);

// spread operator: ...
// Usage: expand array, object .
// example
var parts = ["shoulders", "knees"];
var lyrics = ["head", ...parts, "and", "toes"];

//example
function f(x, y, z) {}
var args = [0, 1, 2];
f(...args);
