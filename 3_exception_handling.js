// try ... catch, throw statements
// ECMAScript expetions and
// DOMEXception and DOMError

// throw expression;
throw "Error2"; // String type
throw 42; // Number type
throw true; // Boolean type
throw {
  toString: function () {
    return "I'm an object!";
  },
};

// Custom Exception ::
// Create an object type UserException
function UserException(message) {
  this.message = message;
  this.name = "UserException";
}

// Make the exception convert to a pretty string when used as a string
// (e.g., by the error console)
UserException.prototype.toString = function () {
  return `${this.name}: "${this.message}"`;
};

// Create an instance of the object type and throw it
throw new UserException("Value too high");

//
// try catch block example
try {
  throw "myException"; // generates an exception
} catch (err) {
  // statements to handle any exceptions
  logMyErrors(err); // pass exception object to error handler
  // Using name and message property of error.
  console.error(err.name);
  console.error(err.message);
} finally {
  doSomething();
}

console.error("Some bug appeared in code.");

// Note: finally block overrides any return, throw statements inside catch statement.

// Using name and message property of error.
throw new Error("Some message");
