// JavaScript is an object-based language based on prototypes,
//  rather than being class-based.
// Prototype-based language. Prototype chain instead of inheritance chain in JS.
// Any JavaScript function can be used as a constructor.
// All objects can inherit from another object.
// Inheritance model : Inherit properties by following the prototype chain.

//
// Our simple object hierarchy to work with :
// Employee : Manager, WorkerBee
// WorkerBee : SalesPerson, Engineer
//

function Employee(name, dept) {
  this.name = name || ""; // if name is false, then second argument , "" is returned.
  this.dept = dept || "general";
}

// Descending from Employee.

function Manager() {
  Employee.call(this);
  this.reports = [];
}
Manager.prototype = Object.create(Employee.prototype);
Manager.prototype.constructor = Manager;

function WorkerBee(name, dept, projs) {
  // Employee.call(this);
  this.base = Employee;
  this.base(name, dept);
  this.projects = projs || [];
}
WorkerBee.prototype = Object.create(Employee.prototype);
// WorkerBee.prototype = new Employee;
WorkerBee.prototype.constructor = WorkerBee;

// Descending from WorkerBee :

function SalesPerson() {
  WorkerBee.call(this);
  this.dept = "sales";
  this.quota = 100;
}
SalesPerson.prototype = Object.create(WorkerBee.prototype);
SalesPerson.prototype.constructor = SalesPerson;

function Engineer(name, projs, mach) {
  // WorkerBee.call(this);
  // this.dept = "engineering";
  this.base = WorkerBee;
  this.base(name, "engineering", projs);
  this.machine = mach || "";
}
Engineer.prototype = Object.create(WorkerBee.prototype);
// Engineer.prototype = new WorkerBee;
Engineer.prototype.constructor = Engineer;

// Creating Objects.
var jim = new Employee("Jim Jones", "marketing");
// Parentheses can be omitted if the
// constructor takes no arguments.

var sally = new Manager();
var mark = new WorkerBee("Mark Smith", "training", ["javascript", "python"]);
var fred = new SalesPerson();
var jane = new Engineer("Jane Doe", ["navigator"], "turbine");

mark.name = "Doe, Mark";
mark.dept = "admin";
mark.projects = ["navigator"];

// Adding properties
// We can add properties to any object at run time in JS.
mark.bonus = 3000; // bonus property is only for mark object.

Employee.prototype.specialty = "none"; // this new property applies to all Employee objects.
Engineer.prototype.specialty = "code"; // overriding value for the Engineer objects.

console.log(
  "jim",
  jim,
  "sally",
  sally,
  "mark",
  mark,
  "fred",
  fred,
  "jane",
  jane,
  jane.specialty
);

// Dynamic inheritance
function Engineer(name, projs, mach) {
  this.base = WorkerBee;
  this.base(name, "engineering", projs);
  this.machine = mach || "";
}
Engineer.prototype = new WorkerBee();
var jane = new Engineer("Doe, Jane", ["navigator", "javascript"], "belau");
Employee.prototype.specialty = "none";
console.log("New jane", jane, jane.specialty);

// Another way of dynamic inheritance : using call() or apply().
function Engineer(name, projs, mach) {
  WorkerBee.call(this, name, "engineering", projs);
  this.machine = mach || "";
}

// Determining instance relationships
console.log("Determining instance relationships");
var chris = new Engineer("Pigman, Chris", ["jsd"], "fiji");
console.log(chris.__proto__ == Engineer.prototype);
console.log(chris.__proto__.__proto__ == WorkerBee.prototype);
console.log(chris.__proto__.__proto__.__proto__ == Employee.prototype);
console.log(chris.__proto__.__proto__.__proto__.__proto__ == Object.prototype);
console.log(chris.__proto__.__proto__.__proto__.__proto__.__proto__ == null);

// Writing instanceof function as follows:
function instanceOf(object, constructor) {
  object = object.__proto__;
  while (object != null) {
    if (object == constructor.prototype) return true;
    if (typeof object == "xml") {
      return constructor.prototype == XML.prototype;
    }
    object = object.__proto__;
  }
  return false;
}
console.log("Custom instanceof function :");
console.log(
  instanceOf(chris, Engineer),
  instanceOf(chris, WorkerBee),
  instanceOf(chris, Employee),
  instanceOf(chris, Object)
);
console.log(instanceOf(chris, SalesPerson));

// Global information in constructors
// Task : Unique ID to be automatically assigned to each new employee .

var idCounter = 1;

function Employee(name, dept) {
  this.name = name || "";
  this.dept = dept || "general";
  if (name) this.id = idCounter++;
}

//
//
// Multiple Inheritance : No multiple inheritance in JS.
// Instead we can have a constructor function call more than one other constructor
// function within it. This gives the illusion of multiple inheritance.

function Hobbyist(hobby) {
  this.hobby = hobby || "scuba";
}

function Engineer(name, projs, mach, hobby) {
  this.base1 = WorkerBee;
  this.base1(name, "engineering", projs);
  this.base2 = Hobbyist;
  this.base2(hobby);
  this.machine = mach || "";
}
Engineer.prototype = new WorkerBee();

var dennis = new Engineer("Doe, Dennis", ["collabra"], "hugo");

// However, assume we then add a property to the Hobbyist constructor's prototype:

Hobbyist.prototype.equipment = ["mask", "fins", "regulator", "bcd"];

// The dennis object does not inherit this new property.
console.log(dennis);
console.log(dennis.__proto__, dennis.hobby);
console.log(dennis.equipment);
