// Regex : useful to match character combinations in string.
// In JS: RegexExp object.
// RegExp methods : exec(), test(),
// string methods:  match(), matchAll(), replace(), search, split().

// creating regular expression
let re = /ab+c/; // pattern enclosed within slashes.
let re2 = new RegExp("ab+c");

let myRe = /d(b+)d/g;
let myArray = myRe.exec("cdbbdbsbz");
console.log(myArray);
