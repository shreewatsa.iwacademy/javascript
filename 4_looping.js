// for loop
for (let step = 0; step < 5; step++) {}

// do...while loop
let step = 1;
do {
  console.log(step);
  step += 1;
} while (step <= 5);

// while loop
while (true) {
  console.log("While Loop");
  break;
}
// labeled statement
topPosition: {
  console.log("At top position !");
  while (true) {
    console.log("outer loop");
    while (true) {
      console.log("inner loop");
      break topPosition;
    }
    continue;
  }
}
// break [label] ;
// continue [label];
// Note: for continue label =>> if label, a looping statement must be identified with that label.

// for...in statement
// do not use for-in statement to loop over arrays. Use traditional for loop instead.
// Iterates over enumerable properties of an object.
// for (variable in object)
//   statement
car = { make: "Ford", model: "Mustang" };
for (let i in car) {
  console.log(car[i]);
}

// for...of statement
// Iterates over iterable objects, including Array, Map, Set, arguments object , etc.
// for (variable of object) {statement}

// Note: for-in iterates over property names, for-of iterates over property values.
const arr = [3, 5, 7];
arr.foo = "hello"; //[3, 5, 7, foo: 'hello' ]

for (let i in arr) {
  console.log(i); // logs "0", "1", "2", "foo"
}

for (let i of arr) {
  console.log(i); // logs 3, 5, 7
}
