// Arrow function is like function expression,
// but does not have its own this, arguments, super, or new.target.
// Arrow functions are always anonymous.

// EXAMPLE 1 : Shorter syntax of arrow function.
let a = ["Hydrogen", "Helium", "Lithium", "Beryllium"];

let a2 = a.map(function (s) {
  return s.length;
});
console.log(a2); // logs [8, 6, 7, 9]

let a3 = a.map((s) => s.length);
console.log(a3); // logs [8, 6, 7, 9]

//EXAMPLE 2 : No this value of its own, takes parent's this.
// In normal functions : assigning the value of this to a variable that could be closed over.
function Person() {
  var self = this; // Some choose `that` instead of `self`.Choose one and be consistent.
  self.age = 0;

  let idvar = setInterval(function growUp() {
    self.age++;

    console.log("Person", self.age);
    if (self.age >= 3) clearInterval(idvar);
  }, 1000);
}
let p = new Person();

// In arrow function.
// An arrow function does not have its own this;
// the this value of the enclosing execution context is used.
function Animal() {
  this.age = 0;

  setInterval(() => {
    this.age++; // |this| properly refers to the animal object
    console.log("Animal", this.age);
  }, 1000);
}

let q = new Animal();
