// The object being inherited from is known as the prototype, and the
// inherited properties can be found in the prototype object of the constructor.

// Property of an object accessed either by its property name or by its ordinal index.
// myCar.color = "red"
// myCar[5] = "25 mpg"

//
// Add a property to a previously defined object type by using the prototype property.
function House(owner) {
  this.owner = owner;
}
House.prototype.color = "white"; // default for all objects.
// house1 = Object.create(House);
house1 = new House("Ram");
house2 = new House("Hari");

house1.color = "Black";
console.log(house1);
console.log(house2);
console.log("Color", house2.color, "\n");

// Defnining methods
var SimpleNum = {
  num: null,
  add: function (other) {
    return this.num + other;
  },
  multiply(other) {
    return this.num * other;
  },
  cube: function () {
    return this.num ** 3;
  },
};

let num1 = Object.create(SimpleNum);
num1.num = 50;
console.log("Number", num1.num);
console.log("Add", num1.add(40));

function square() {
  return this.num ** 2;
}
function divide(other) {
  return this.num / other;
}

SimpleNum.square = square;
num1.divide = divide;

console.log("Square", num1.square());
console.log("Divide", num1.divide(5));
console.log("Cube", num1.cube());

//
// Getters and Setters : Use prefix get and set.
var o = {
  a: 7,
  get b() {
    return this.a + 1;
  },
  set c(x) {
    this.a = x / 2;
  },
};

console.log(o.a); // 7
console.log(o.b); // 8 <-- At this point the get b() method is initiated.
o.c = 50; //   <-- At this point the set c(x) method is initiated
console.log(o.a); // 25

// Object.defineProperties() : Add getters/setters after creation of object.
Object.defineProperties(o, {
  d: {
    get: function () {
      return this.a * 2;
    },
  },
  e: {
    set: function (x) {
      this.a = this.a + 2 * x;
    },
  },
});

o.e = 10; // Runs the setter e(x).
console.log(o.d); // Runs the getter, which yields a * 2 or 90

//
// Deleting properties
// delete non-inherited property using delete operator.
// Creates a new object, myobj, with two properties, a and b.
var myobj = new Object();
myobj.a = 5;
myobj.b = 12;

// Removes the a property, leaving myobj with only the b property.
delete myobj.a;
console.log("a" in myobj); // output: "false"

// If var keyword was not used , then we can delete a globle variable
// with delete operator too.
g = 17;
delete g;

//
// Comparing objects : every object has unique reference, so no object are equal.
// Two variables, two distinct objects with the same properties
var fruit = { name: "apple" };
var fruitbear = { name: "apple" };

fruit == fruitbear; // return false
fruit === fruitbear; // return false

// Two variables, a single object
var fruit = { name: "apple" };
var fruitbear = fruit; // Assign fruit object reference to fruitbear

// Here fruit and fruitbear are pointing to same object
fruit == fruitbear; // return true
fruit === fruitbear; // return true

fruit.name = "grape";
console.log(fruitbear); // output: { name: "grape" }, instead of { name: "apple" }
