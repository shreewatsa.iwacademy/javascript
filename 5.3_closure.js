// Most commonly, closures are the nested functions.
// The inner function forms a closure:
// the inner function can use the arguments and variables of the outer function,
// while the outer function cannot use the arguments and variables of the inner function.

// EXAMPLE 1
function addSquares(a, b) {
  function square(x) {
    return x * x;
  }
  return square(a) + square(b);
}
a = addSquares(2, 3); // returns 13
console.log(a);

//EXAMPLE 2
// Noteice , how x is preserved when inside function is returned.
// Scope chain : first inside, then outside, then global object.
function outside(x) {
  function inside(y) {
    return x + y;
  }
  return inside;
}
fn_inside = outside(3);
// Think of it like: give me a function that adds 3 to whatever you give it .
result = fn_inside(5); // returns 8
console.log(result);
result1 = outside(3)(5); // returns 8
console.log(result1);

// EXAMPLE 3
// Multiple nested functions.
// Scope chaining.
// Since C can access B which can access A, C can also access A .
function A(x) {
  function B(y) {
    function C(z) {
      console.log(x + y + z);
    }
    C(3);
  }
  B(2);
}
A(1); // logs 6 (1 + 2 + 3)

// Closure
// EXAMPLE 4
var pet = function (name) {
  // The outer function defines a variable called "name"
  var getName = function () {
    return name; // The inner function has access to the "name" variable of the outer
    //function
  };
  return getName; // Return the inner function, thereby exposing it to outer scopes
};
myPet = pet("Vivie");

console.log(myPet()); // Logs "Vivie"

// Closure
// EXAMPLE 5
var createPet = function (name) {
  var sex;

  return {
    setName: function (newName) {
      name = newName;
    },

    getName: function () {
      return name;
    },

    getSex: function () {
      return sex;
    },

    setSex: function (newSex) {
      if (
        typeof newSex === "string" &&
        (newSex.toLowerCase() === "male" || newSex.toLowerCase() === "female")
      ) {
        sex = newSex;
      }
    },
  };
};

var pet = createPet("Vivie");
pet.getName(); // Vivie

pet.setName("Oliver");
pet.setSex("male");
pet.getSex(); // male
pet.getName(); // Oliver

// EXAMPLE 6
// Multiple/ Dynamic number of parameters
// Using arguments object : an array like object that contains arguments passed to function.
function myConcat(separator) {
  let result = "";
  for (let i = 1; i < arguments.length; i++) {
    result += arguments[i] + separator;
  }
  console.log(result);
  return result;
}
myConcat(", ", "red", "blue", "orange");

// EXAMPLE 7
// default parameters and rest parameters
// typeof 12
(function (a, b = "b") {
  console.log(a, b);
})();

// rest parameter : ...args , here args is an array.
res = (function multiply(multiplier, ...args) {
  return args.map((x) => multiplier * x);
})(2, 1, 2, 3, 4, 5);
console.log(res);
