// Map, Set .
// Map : maps values to values.
// Map object : key/value map and can iterate its elements in insertion order.
// Use for...of loop to return an array of key:value for each iteration.
// Note: key can be any value for a map, unlike Object (only Strings and Symbols).

let sayings = new Map();
sayings.set("dog", "woof");
sayings.set("cat", "meow");
sayings.set("elephant", "toot");
sayings.size; // 3
sayings.get("fox"); // undefined
sayings.has("bird"); // false
sayings.delete("dog");
sayings.has("dog"); // false
sayings.keys(); // { 'cat', 'elephant' }
sayings.values(); //{ 'meow', 'toot' }

for (let [key, value] of sayings) {
  console.log(key + " goes " + value);
}
// "cat goes meow"
// "elephant goes toot"

sayings.clear();
sayings.size; // 0

// objWithNoDefaultKeys = Object.create(null)

//
// WeakMap object : key/value pairs
// key are objects only and values can be arbitrary values.
// The object references in the keys are held weakly, meaning that they are a target
// of garbage collection (GC) if there is no other reference to the object anymore.
// keys are not enumerable. ie no .keys() method.

// One of the Use case: to store private data for an object , or to hide
// implementation details.

const privates = new WeakMap();

function Public() {
  const me = {
    // Private data goes here
  };
  privates.set(this, me);
}

Public.prototype.method = function () {
  const me = privates.get(this);
  // Do stuff with private data in `me`...
};

module.exports = Public;

//
// Set object: collection of values.
// can iterate elements in insertion order.
// each value is unique.
let mySet = new Set();
mySet.add(1);
mySet.add("some text");
mySet.add("foo");

mySet.has(1); // true
mySet.delete("foo");
mySet.size; // 2

for (let item of mySet) console.log(item);
// 1
// "some text"

// converting set to array .
Array.from(mySet);
// or
[...mySet];

// converting array to set
myset2 = new Set([1, 2, 3, 4, 5]);

// Deleting array elements by value
// arr.splice(arr.indexOf(val), 1)

// WeakSet object :
// like Set, but  collection of objects only and not enumerable.
