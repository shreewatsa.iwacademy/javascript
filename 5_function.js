// Note: functin hoisting only works with function declarations, not with function expressions.
// Function are also objects in javascript.
// Function declaration

var mycar = { make: "Honda", model: "Accord", year: 1998 };
function myFunc(theObject) {
  theObject.make = "Toyota";
}
console.log(mycar);
myFunc(mycar);
console.log(mycar);

// Function expression

const square = function (number) {
  return number * number;
};
const cube = function c(n) {
  return n * n * n;
};
var x = square(4);
var y = cube(4);
console.log(x, y);

// passing function as an argument
function map(f, a) {
  let result = [];
  for (let i = 0; i < a.length; i++) {
    result[i] = f(a[i]);
  }
  return result;
}
console.log(map(square, [1, 2, 3, 4, 5]));

// Recursive
function factorial(n) {
  if (n === 0 || n === 1) return 1;
  else return n * factorial(n - 1);
}
console.log(factorial(5));
