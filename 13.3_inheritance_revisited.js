// __proto__ property for prototype chain.
function Employee() {
  this.name = "";
  this.dept = "general";
}

function WorkerBee() {
  this.projects = [];
}
WorkerBee.prototype = new Employee();

var amy = new WorkerBee();
console.log(amy);
console.log(amy.__proto__);

Employee.prototype.name = "Unknown";
console.log("Name : ", amy.name);

// Change the value of an object property at run time and have the new value
// be inherited by all descendants of the object
console.log("Changing properties dynamically !!");

function Employee() {
  this.dept = "general"; // Note that this.name (a local variable) does not appear here
}
Employee.prototype.name = ""; // A single copy

function WorkerBee() {
  this.projects = [];
}
WorkerBee.prototype = new Employee();

var amy = new WorkerBee();
console.log(amy.__proto__, amy.name);
Employee.prototype.name = "Unknown";
console.log(amy.__proto__, amy.name);

//
// Determining instance relationships
// Lookup in the prototype chain
// __proto__
// Testing inheritance by comparing an object's __proto__ to a function's prototype object.
// The expression new Foo() creates an object with __proto__ == Foo.prototype .
// instanceof() operator
function Foo() {}
var f = new Foo();
var isTrue = f instanceof Foo;
console.log(isTrue);
