// Working with string and text in JS.
// each character : 16 bit unsigned integer values.

console.log("\xA9", "\u{A9}"); // \x : hexadecimal sequence, \u : unicode sequence.

// String object : a wrapper around string primitive data type.
const foo = new String("foo"); // Creates a String object
console.log(foo); // Displays: [String: 'foo']
typeof foo; // Returns 'object'
console.log("hello".length);

// string methods:
// charAt, indexOf, lastIndexOf, startsWith, endsWith, includes,
// split, slice, substring, substr,
// match, matchAll, replace, search,
// toLowerCase, toUpperCase,
// repeat, trim

// Multiline literals OR Template literals : ` This is a template literal `
// allows string interpolation

console.log(`This is a 
multiline
string and it is a nice feature. `);

const five = 5;
const ten = 10;
console.log(`Fifteen is ${five + ten} and not ${2 * five + ten}.`);

// Internationalization : Intl object
// Language sensitive string comparison, number formatting, and date and time formatting.
// properties of Intl object: Collator, NumberFormat, and DateTimeFormat.

// DateTimeFormat : formatting date and time.
const msPerDay = 24 * 60 * 60 * 1000;
const july172014 = new Date(msPerDay * (44 * 365 + 11 + 197)); // July 17, 2014 00:00:00 UTC.

const options = {
  year: "2-digit",
  month: "2-digit",
  day: "2-digit",
  hour: "2-digit",
  minute: "2-digit",
  timeZoneName: "short",
  timeZone: "UTC",
};
const americanDateTime = new Intl.DateTimeFormat("en-US", options).format;
const portugueseDateTime = new Intl.DateTimeFormat(["pt-BR", "pt-PT"], options)
  .format;

console.log(americanDateTime(july172014)); // 07/16/14, 5:00 PM PDT
console.log(portugueseDateTime(july172014));

// NumberFormat : formatting numbers.
const gasPrice = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2,
});

console.log(gasPrice.format(5.259)); // $5.259

const hanDecimalRMBInChina = new Intl.NumberFormat("zh-CN-u-nu-hanidec", {
  style: "currency",
  currency: "CNY",
});

console.log(hanDecimalRMBInChina.format(1314.25)); // ￥ 一,三一四.二五

// Collator : for comparing and sorting strings.
const names = ["Hochberg", "Hönigswald", "Holzman"];

const germanPhonebook = new Intl.Collator("de-DE-u-co-phonebk");

// as if sorting ["Hochberg", "Hoenigswald", "Holzman"]:
console.log(names.sort(germanPhonebook.compare).join(", "));
// logs "Hochberg, Hönigswald, Holzman"
