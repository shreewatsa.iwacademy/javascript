// If ... else
/*
if (condition_1) {
  statement_1;
} else if (condition_2) {
  statement_2;
} else if (condition_n) {
  statement_n;
} else {
  statement_last;
} 
*/

x = false;
if (x) console.log("true");
else console.log("false");

y = true;
// Using assignment in a conditional expression.
if ((x = y)) {
  console.log("inside if");
} else {
  console.log("inside else");
}

// Falsy values: false, undefined, null, 0, NaN, "" .

// Switch statement
/* 
switch (expression) {
  case label_1:
    statements_1
    [break;]
  case label_2:
    statements_2
    [break;]
    …
  default:
    statements_def
    [break;]
}
*/
