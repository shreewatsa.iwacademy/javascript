// Note: DO NOT WRITE decimal numbers with LEADING ZERO. It may conflict between octal and decimal.
// Numbers
// double precision 64-bits numbers in javascript
// 53 precision bits
// +Infinity, -Infinity, NaN, BigInt

// Four types of number literals : decimal, binary, octal, hexadecimal.
typeof Boolean; // function
typeof Boolean(); // boolean
typeof new Boolean(); // object

console.log(0644 === 420); // true

// Number object.
// methods : parseFloat, parseInt, isFinite, isInteger, isNaN, etc.
let numberProperties = {
  biggestNum: Number.MAX_VALUE,
  smallestNum: Number.MIN_VALUE,
  infiniteNum: Number.POSITIVE_INFINITY,
  negInfiniteNum: Number.NEGATIVE_INFINITY,
  notANum: Number.NaN,
  epsilon: Number.EPSILON,
  minSafeNum: Number.MIN_SAFE_INTEGER,
  maxSafeNum: Number.MAX_SAFE_INTEGER,
};

// methods of Number.prototype : toExponential(), toFixed(), toPrecision().
x = Number.prototype;
x = 10 ** 50;
console.log(x.toExponential());

//
// Math object

// has properties and methods for mathematical constants and functions.
// Includes trigonometric, logarithmic, exponential, and other functions.
console.log(Math.PI);
console.log(Math.sin(1.56));
console.log(Math.abs(-563.23));

// Useful methods: pow, exp, log10, log2, floor, ceil, min, max, random, round,
// fround, trunc, sqrt, cbrt, sign, etc.

//
// Date object

// Doesnot have any properties.
// has methods for setting, getting, and manipulating dates.
// Timestamp being the number of seconds since January 1, 1970, 00:00:00.
// Create a Date object.
// var dateObjectName = new Date([parameters]);

today = new Date();
let xmas_95 = new Date("December 25, 1995 13:30:00");
let xmas_96 = new Date(1996, 11, 25);
let xmas_97 = new Date(1997, 11, 25, 9, 30, 0);
console.log({ today, xmas_95, xmas_96, xmas_97 });

// Date methods : get, set, to, parse and UTC methods.
// Day : 0(Sun) to 6(Sat)
// Month : 0(Jan) to 11(Dec)

xmas_95.getMonth();
// getTime() returns the number of milliseconds since January 1, 1970 for a Date object.
console.log(xmas_95.getTime());

var today = new Date();
var endYear = new Date(1995, 11, 31, 23, 59, 59, 999); // Set day and month
endYear.setFullYear(today.getFullYear()); // Set year to this year
var msPerDay = 24 * 60 * 60 * 1000; // Number of milliseconds per day
var daysLeft = (endYear.getTime() - today.getTime()) / msPerDay;
var daysLeft = Math.round(daysLeft); //returns days left in the year

console.log({ today, endYear, msPerDay, daysLeft });

// Date.parse() to parse date from date string.
var IPOdate = new Date();
IPOdate.setTime(Date.parse("Aug 9, 1995"));

// JSClock : digital clock example.
(function JSClock() {
  var time = new Date();
  var hour = time.getHours();
  var minute = time.getMinutes();
  var second = time.getSeconds();
  var temp = "" + (hour > 12 ? hour - 12 : hour);
  if (hour == 0) temp = "12";
  temp += (minute < 10 ? ":0" : ":") + minute;
  temp += (second < 10 ? ":0" : ":") + second;
  temp += hour >= 12 ? " P.M." : " A.M.";
  console.log(temp);
  return temp;
})();
