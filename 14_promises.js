// A Promise is an object representing the eventual completion or
// failure of an asynchronous operation

// To execute two or more asynchronous operations back to back : use promise chain.

// function doSomething() {}
// function successCallback() {}
// function failureCallback() {}

// const promise = doSomething();
// const promise2 = promise.then(successCallback, failureCallback);

// or , shorthand is :
// const promise2 = doSomething().then(successCallback, failureCallback);

// promise chaining

// doSomething()
// .then(function(result) {
//   return doSomethingElse(result);
// })
// .then(function(newResult) {
//   return doThirdThing(newResult);
// })
// .then(function(finalResult) {
//   console.log('Got the final result: ' + finalResult);
// })
// .catch(failureCallback);

// Note: The arguments to then are optional,
// and catch(failureCallback) is short for then(null, failureCallback)

// with arrow functions:

// doSomething()
// .then(result => doSomethingElse(result))
// .then(newResult => doThirdThing(newResult))
// .then(finalResult => {
//   console.log(`Got the final result: ${finalResult}`);
// })
// .catch(failureCallback);

// With arrow functions () => x is short for () => { return x; } .

// EXAMPLE 1
new Promise((resolve, reject) => {
  console.log("Initial");

  resolve();
})
  .then(() => {
    throw new Error("Something failed"); // this caused a rejection.

    console.log("Do this");
  })
  .catch(() => {
    console.error("Do that");
  })
  .then(() => {
    console.log("Do this, no matter what happened before");
  });

//
// Using async / await.
//
// async function foo() {
//   try {
//     const result = await doSomething();
//     const newResult = await doSomethingElse(result);
//     const finalResult = await doThirdThing(newResult);
//     console.log(`Got the final result: ${finalResult}`);
//   } catch(error) {
//     failureCallback(error);
//   }
// }

// Note: Promises catches all errors, thrown exceptions and programming errors.

// Promise rejection events: rejectionhandled , unhandledrejection.
// rejection event has promise property and reason property .

// window.addEventListener(
//   "unhandledrejection",
//   (event) => {
//     /* You might start here by adding code to examine the
//      promise specified by event.promise and the reason in
//      event.reason */

//     event.preventDefault();
//   },
//   false
// );

// Creating a promise.
function saySomething(str) {
  console.log(str);
  return "helloworld";
}
setTimeout(() => saySomething("5 seconds passed"), 5 * 1000);

// Best practice :
var wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

wait(10 * 1000)
  .then(() => saySomething("10 seconds"))
  .catch(console.log("error occured"));

// Composition
// Promise.resolve() and Promise.reject()
// Promise.all() and Promise.race() : run asynchronous operations in parallel.

// Start operations in parallel and wait for all of them to finish :
// Promise.all([func1(), func2(), func3()])
// .then(([result1, result2, result3]) => { /* use result1, result2 and result3 */ });

// Sequential composition is done like this:
// [func1, func2, func3].reduce((p, f) => p.then(f), Promise.resolve())
// .then(result3 => { /* use result3 */ });

// This is equivalent to :
// Promise.resolve().then(func1).then(func2).then(func3);

// Or more simply using async/await:
// let result;
// for (const f of [func1, func2, func3]) {
//   result = await f(result);
// }
// /* use last result (i.e. result3) */

// Timing
// Functions passed to then() will never be called synchronously.
Promise.resolve().then(() => console.log(2));
console.log(1); // 1, 2

// Another example.
var wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

wait().then(() => console.log(40));
Promise.resolve()
  .then(() => console.log(20))
  .then(() => console.log(30));
console.log(10); // 10, 20, 30, 40

// Nesting

// doSomethingCritical()
// .then(result => doSomethingOptional(result)
//   .then(optionalResult => doSomethingExtraNice(optionalResult))
//   .catch(e => {})) // Ignore if optional stuff fails; proceed.
// .then(() => moreCriticalStuff())
// .catch(e => console.error("Critical failure: " + e.message));

// Common mistakes.
// // Bad example! Spot 3 mistakes!

// doSomething().then(function(result) {
//   doSomethingElse(result) // Forgot to return promise from inner chain + unnecessary nesting
//   .then(newResult => doThirdThing(newResult));
// }).then(() => doFourthThing());
// // Forgot to terminate chain with a catch!

// Code with removed bugs of above example.
// doSomething()
// .then(function(result) {
//   return doSomethingElse(result);
// })
// .then(newResult => doThirdThing(newResult))
// .then(() => doFourthThing())
// .catch(error => console.error(error));
